const String degree = "°";
const String apiKey = "05639a314e1d398b690956c529bf9195";
const String currentEndpoint =
    "https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}";
const String daysEndpoint =
    "https://api.openweathermap.org/data/2.5/forecast?lat={lat}&lon={lon}&appid={API key}";
const String latitude = "21.0288157";
const String longtitude = "105.7879849";
