import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:haizzzzz/constants/colors.dart';
import 'package:haizzzzz/constants/images.dart';
import 'package:haizzzzz/constants/strings.dart';
import 'package:haizzzzz/controller/maincontroller.dart';
import 'package:haizzzzz/models/current_weather_model.dart';
import 'package:haizzzzz/models/hourly_weather_model.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var date = DateFormat.yMd().add_jm().format(DateTime.now());
    var theme = Theme.of(context);
    var controller = Get.put(MainController());
    return Scaffold(
      backgroundColor: theme.scaffoldBackgroundColor,
      appBar: AppBar(
          elevation: 0.0,
          title: date.text.gray400.make(),
          backgroundColor: Colors.transparent,
          actions: [
            Obx(
              () => IconButton(
                onPressed: () {
                  controller.changeTheme();
                },
                icon: Icon(
                  controller.isDark.value ? Icons.light_mode : Icons.dark_mode,
                  color: theme.iconTheme.color,
                ),
                color: theme.iconTheme.color,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.more_vert),
              color: Vx.amber400,
            )
          ]),
      body: Obx(() => controller.isloaded.value == true
          ? Container(
              padding: const EdgeInsets.all(12),
              child: FutureBuilder(
                  future: controller.currentWeatherData,
                  builder: (BuildContext context, AsyncSnapshot snapshot) {
                    if (snapshot.hasData) {
                      CurrentWeatherData data = snapshot.data;
                      return SingleChildScrollView(
                        physics: const BouncingScrollPhysics(),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              "${data.name}"
                                  .text
                                  .uppercase
                                  .fontFamily('poppin_bold')
                                  .size(30)
                                  .fontWeight(FontWeight.bold)
                                  .letterSpacing(3)
                                  .color(theme.primaryColor)
                                  .make(),
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset(
                                      "assets/weather/${data.weather?[0].icon}.png"),
                                  RichText(
                                    text: TextSpan(children: [
                                      TextSpan(
                                          text: "${data.main?.temp}",
                                          style: TextStyle(
                                              color: theme.primaryColor,
                                              fontSize: 64,
                                              fontFamily: "poppin")),
                                      TextSpan(
                                          text: "${data.weather![0].main}",
                                          style: TextStyle(
                                              color: theme.primaryColor,
                                              letterSpacing: 3,
                                              fontSize: 20,
                                              fontFamily: "poppin")),
                                    ]),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  TextButton.icon(
                                      onPressed: null,
                                      icon: Icon(
                                        Icons.expand_less_outlined,
                                        color: theme.primaryColor,
                                      ),
                                      label: "${data.main?.tempMax}$degree"
                                          .text
                                          .color(theme.primaryColor)
                                          .make()),
                                  TextButton.icon(
                                      onPressed: null,
                                      icon: Icon(
                                        Icons.expand_more_outlined,
                                        color: theme.primaryColor,
                                      ),
                                      label: "${data.main?.tempMin}$degree"
                                          .text
                                          .color(theme.primaryColor)
                                          .make()),
                                ],
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: List.generate(3, (index) {
                                  var iconList = [clouds, humidity, windspeed];
                                  var values = [
                                    "${data.clouds!.all}",
                                    "${data.main!.humidity} %",
                                    "${data.wind!.speed} km/h"
                                  ];

                                  return Column(
                                    children: [
                                      Image.asset(
                                        iconList[index],
                                        width: 60,
                                        height: 60,
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      values[index]
                                          .text
                                          .black
                                          .color(theme.primaryColor)
                                          .make()
                                    ],
                                  );
                                }),
                              ),
                              10.heightBox,
                              const Divider(),
                              10.heightBox,
                              FutureBuilder(
                                  future: controller.hourlyWeatherData,
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (snapshot.hasData) {
                                      HourlyWeatherData hourlyWeatherData =
                                          snapshot.data;
                                      return SizedBox(
                                        height: 180,
                                        child: ListView.builder(
                                            physics:
                                                const BouncingScrollPhysics(),
                                            scrollDirection: Axis.horizontal,
                                            shrinkWrap: true,
                                            itemCount:
                                                hourlyWeatherData.list!.length >
                                                        6
                                                    ? 6
                                                    : hourlyWeatherData
                                                        .list!.length,
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              var time = DateFormat.jm().format(
                                                  DateTime
                                                      .fromMillisecondsSinceEpoch(
                                                          hourlyWeatherData
                                                                  .list![index]
                                                                  .dt!
                                                                  .toInt() *
                                                              1000));
                                              return Container(
                                                padding:
                                                    const EdgeInsets.all(15),
                                                margin: const EdgeInsets.only(
                                                    right: 5),
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            12),
                                                    color: cardColor),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          0, 10, 0, 0),
                                                  child: Column(children: [
                                                    time.text.make(),
                                                    Image.asset(
                                                      "assets/weather/${hourlyWeatherData.list![index].weather![0].icon}.png",
                                                      width: 80,
                                                    ),
                                                    10.heightBox,
                                                    "${hourlyWeatherData.list![index].main!.temp}$degree C"
                                                        .text
                                                        .make(),
                                                  ]),
                                                ),
                                              );
                                            }),
                                      );
                                    } else {
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                  }),
                              10.heightBox,
                              const Divider(),
                              10.heightBox,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  "Next 7 day"
                                      .text
                                      .semiBold
                                      .size(16)
                                      .color(theme.primaryColor)
                                      .make(),
                                  TextButton(
                                      onPressed: null,
                                      child: "View All"
                                          .text
                                          .semiBold
                                          .color(theme.primaryColor)
                                          .make())
                                ],
                              ),
                              10.heightBox,
                              const Divider(),
                              10.heightBox,
                              FutureBuilder(
                                  future: controller.hourlyWeatherData,
                                  builder: (BuildContext context,
                                      AsyncSnapshot snapshot) {
                                    if (snapshot.hasData) {
                                      HourlyWeatherData hourlyData =
                                          snapshot.data;
                                      return ListView.builder(
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        shrinkWrap: true,
                                        itemCount: 7,
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          var day = DateFormat("EEEE").format(
                                              DateTime.now().add(
                                                  Duration(days: index + 1)));
                                          return Card(
                                            child: Container(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 12),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Expanded(
                                                      child: day.text.semiBold
                                                          .color(theme
                                                              .primaryColor)
                                                          .make()),
                                                  Expanded(
                                                    child: TextButton.icon(
                                                        onPressed: null,
                                                        icon: Image.asset(
                                                            "assets/weather/${hourlyData.list![index].weather![0].icon}.png",
                                                            width: 40),
                                                        label:
                                                            "${hourlyData.list?[index].main?.tempMax}$degree"
                                                                .text
                                                                .size(16)
                                                                .color(theme
                                                                    .primaryColor)
                                                                .make()),
                                                  ),
                                                  RichText(
                                                    text: TextSpan(
                                                      children: [
                                                        TextSpan(
                                                            text:
                                                                "${hourlyData.list?[index].main?.tempMax}$degree /",
                                                            style: TextStyle(
                                                              color: theme
                                                                  .primaryColor,
                                                              fontFamily:
                                                                  "poppins",
                                                              fontSize: 16,
                                                            )),
                                                        TextSpan(
                                                            text:
                                                                "${hourlyData.list?[index].main?.tempMin}$degree",
                                                            style: TextStyle(
                                                              color: theme
                                                                  .iconTheme
                                                                  .color,
                                                              fontFamily:
                                                                  "poppins",
                                                              fontSize: 16,
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    } else {
                                      return const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                  })
                            ]),
                      );
                    } else {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }))
          : const Center(
              child: CircularProgressIndicator(),
            )),
    );
  }
}
