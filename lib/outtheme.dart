import 'package:flutter/material.dart';
import 'package:haizzzzz/constants/colors.dart';
import 'package:velocity_x/velocity_x.dart';

class CustomThemes {
  static final lightTheme = ThemeData(
      fontFamily: "poppin",
      scaffoldBackgroundColor: Colors.white,
      primaryColor: Vx.gray800,
      iconTheme: const IconThemeData(color: Vx.gray600));

  static final darkTheme = ThemeData(
      fontFamily: "poppin",
      scaffoldBackgroundColor: bgColor,
      primaryColor: Vx.white,
      iconTheme: const IconThemeData(color: Colors.white));
}
