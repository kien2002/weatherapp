import 'package:haizzzzz/constants/strings.dart';
import 'package:haizzzzz/models/current_weather_model.dart';
import 'package:haizzzzz/models/hourly_weather_model.dart';
import 'package:http/http.dart' as http;

var apiUrl =
    "https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longtitude&appid=$apiKey&units=metric";
var hourlyUrl =
    "https://api.openweathermap.org/data/2.5/forecast?lat=$latitude&lon=$longtitude&appid=$apiKey&units=metric";

getCurrentWeather(lat, long) async {
  var apiUrl =
      "https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$long&appid=$apiKey&units=metric";
  var res = await http.get(Uri.parse(apiUrl));
  if (res.statusCode == 200) {
    var data = currentWeatherDataFromJson(res.body.toString());
    return data;
  }
}

getHourlyWeather(lat, long) async {
  var hourlyUrl =
      "https://api.openweathermap.org/data/2.5/forecast?lat=$lat&lon=$long&appid=$apiKey&units=metric";
  var res = await http.get(Uri.parse(hourlyUrl));
  if (res.statusCode == 200) {
    var data = hourlyWeatherDataFromJson(res.body.toString());
    return data;
  }
}
